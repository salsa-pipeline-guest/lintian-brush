Contributing
============

Coding Style
------------

lintian-brush uses PEP8 as its coding style.

Code style can be checked by running ``flake8``:

```shell
flake8
```

Tests
-----

To run the testsuite, use:

```shell
python3 setup.py test
```

The tests are also run by the package build and autopkgtest.
